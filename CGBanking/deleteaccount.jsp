<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" session="true"%>
<%@page import="com.cg.banking.beans.Customer"%>
<%@page import="com.cg.banking.beans.Account"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fo" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete Account</title>

<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<link rel="stylesheet" href="resources/assets/css/main.css" />
<noscript><link rel="stylesheet" href="resources/assets/css/noscript.css" /></noscript>

</head>
<body>
	
		<%
			List<Account> allAccounts = (List<Account>) session.getAttribute("accountlist");
			Customer customer=(Customer)session.getAttribute("customer");
		%>
		
		<div id="page-wrapper">

						<!-- Wrapper -->
							<div id="wrapper">
							
							<div class="span-3">
							
							<section class="panel color2-alt">
												<h3 class="major">Deposit</h3>
												<fo:form action="deleteAccount" method="post">
													<div class="table-wrapper">
														<table>
															<thead>
																<tr>
																	<th></th>
																</tr>
																<tr>
																	<th></th>
																	<th></th>
																</tr>
																<tr>
																	<th>Customer ID</th>
																	<th><%=customer.getCustomerId()%></th>
																	<th>						</th>													
																</tr>
																
																<tr>
																	<th>Customer Name</th>
																	<th><%=customer.getFirstName()+" "+customer.getLastName()%></th>														
																</tr>
																
															</thead>
															<tbody>
																<tr>
																	<td>Email</td>
																	<td><%=customer.getEmailId()%></td>
																</tr>
																
																<tr>
																	<td>PanCard</td>
																	<td><%=customer.getPanCard()%></td>
																</tr>

																<tr>
																	<td>Password</td>
																	<td><%=customer.getPassword()%></td>
																</tr>																
																
																<tr>
																	<td>Choose Account</td>
																	<td>
																		<div class="field third">
																			<div class="select-wrapper">
																				<select name="accountNo"> 
																					<c:forEach var="accounts" items="<%=allAccounts%>">
																						<option value="${accounts.accountNo}">${accounts.accountNo}</option>
																					</c:forEach>
																				</select>
																			</div>
																		</div>
																	</td>
																</tr>
														
																</tbody>
																
														</table>
														
																		<ul class="actions">
																			<li><input type="submit" value="Delete Account" class="special color2" /></li>
																		</ul>
														
													</fo:form>
												</div>
											</section>
										</div>
								</div>
						</div>
						
						<!-- Scripts -->
							<script src="resources/assets/js/jquery.min.js"></script>
							<script src="resources/assets/js/skel.min.js"></script>
							<script src="resources/assets/js/main.js"></script>

</body>
</html>